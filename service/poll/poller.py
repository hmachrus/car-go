import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here.
# from service_rest.models import Something

from service_rest.models import ManufacturerVO, VehicleModelVO, AutomobileVO


def get_manufacturer():
    response = requests.get("http://localhost:8100/api/manufacturers/")
    content = json.loads(response.content)
    for manufacturer in content["manufacturers"]:
        ManufacturerVO.objects.update_or_create(
            import_href=manufacturer["href"],
            defaults={
            "name": manufacturer["name"],
             }
        )

def get_vehiclemodel():
    response = requests.get("http://localhost:8100/api/models/")
    content = json.loads(response.content)
    for model in content["models"]:
        VehicleModelVO.objects.update_or_create(
            import_href=model["href"],
            defaults={
            "name": model["name"],
            "picture_url": model["picture_url"],
            "manufacturer_id": model["manufacturer_id"],
             }
        )


def get_automobile():
    response = requests.get("http://localhost:8100/api/automobiles/")
    content = json.loads(response.content)
    for automobile in content["automobiles"]:
        AutomobileVO.objects.update_or_create(
            import_href=automobile["href"],
            defaults={
            "color": automobile["color"],
            "year": automobile["year"],
            "vin": automobile["vin"],
            "model_id": automobile["model_id"]
             }
        )



def poll():
    while True:
        print('Service poller polling for data')
        try:
            get_automobile()
            get_manufacturer()
            get_vehiclemodel()
            # Write your polling logic, here
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
