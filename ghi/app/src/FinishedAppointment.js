import React from 'react';
import './index.css';

function FinishedAppt(props) {
	const handleSubmit = async (event) => {
		const locationUrl = `http://localhost:8080/api/services/${props.id}`;
		const fetchConfig = {
			method: 'DELETE',
			body: JSON.stringify(props.id),
			headers: {
				'Content-Type': 'application/json',
			},
		};
		try {
			const response = await fetch(locationUrl, fetchConfig);
		} catch (error) {
			console.log(error);
		}
	};
	return (
		<button className="FinishedButton" onClick={handleSubmit}>
			Finished Appointment
		</button>
	);
}

export default FinishedAppt;
