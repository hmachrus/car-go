import React from 'react';
import './index.css';

function DeleteTech(props) {
	const handleSubmit = async (event) => {
		const locationUrl = `http://localhost:8080/api/techs/${props.id}`;
		const fetchConfig = {
			method: 'DELETE',
			body: JSON.stringify(props.id),
			headers: {
				'Content-Type': 'application/json',
			},
		};
		try {
			const response = await fetch(locationUrl, fetchConfig);
		} catch (error) {
			console.log(error);
		}
	};
	return (
		<button className="DeleteButton" onClick={handleSubmit}>
			Delete Tech
		</button>
	);
}

export default DeleteTech;
