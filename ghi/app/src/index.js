import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
// 	<React.StrictMode>
// 		<App />
// 	</React.StrictMode>
// );

async function loadTechs() {
	const response = await fetch('http://localhost:8080/api/techs/');
	const response1 = await fetch('http://localhost:8080/api/services/');
	const response2 = await fetch('http://localhost:8100/api/automobiles/');
	const response3 = await fetch('http://localhost:8100/api/models/');
	if (response.ok) {
		const data = await response.json();
		const data1 = await response1.json();
		const data2 = await response2.json();
		const data3 = await response3.json();
		root.render(
			<React.StrictMode>
				<App
					techs={data.techs}
					services={data1.services}
					autos={data2.autos}
					models={data3.models}
				/>
			</React.StrictMode>
		);
	} else {
		console.error(response);
	}
}
loadTechs();

// async function loadAppointments() {
// 	const response = await fetch('http://localhost:8080/api/services/');
// 	if (response.ok) {
// 		const data = await response.json();
// 		root.render(
// 			<React.StrictMode>
// 				<App services={data.services} />
// 			</React.StrictMode>
// 		);
// 	} else {
// 		console.error(response);
// 	}
// }
// loadAppointments();
